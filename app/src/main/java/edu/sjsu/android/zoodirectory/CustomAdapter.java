package edu.sjsu.android.zoodirectory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private ArrayList<Cat> catList;
    private OnCatListener mOnCatListener;
    Context context;

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param catList ArrayList<> containing the data to populate views to be used
     * by RecyclerView.
     */
    public CustomAdapter(Context context, ArrayList<Cat> catList, OnCatListener onCatListener) {
        this.catList = catList;
        this.mOnCatListener = onCatListener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_layout, parent, false);

        return new ViewHolder(view, mOnCatListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.getHeader().setText(catList.get(position).getName());
        viewHolder.getImage().setImageResource(catList.get(position).getImageID());
    }

    @Override
    public int getItemCount() {
        return catList.size();
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView header;
        private final ImageView img;
        private OnCatListener listener;

        public ViewHolder(View view, OnCatListener onCatListener) {
            super(view);
            // Define click listener for the ViewHolder's View

            header = (TextView) view.findViewById(R.id.headerTextView);
            img = (ImageView) view.findViewById(R.id.iconView);
            listener = (OnCatListener) onCatListener;
            view.setOnClickListener(this);
        }

        public TextView getHeader() {
            return this.header;
        }

        public ImageView getImage() {
            return img;
        }

        @Override
        public void onClick(View view) {
            listener.onCatClick(getAdapterPosition());
        }
    }

    public interface OnCatListener {
        void onCatClick(int position);
    }
}
