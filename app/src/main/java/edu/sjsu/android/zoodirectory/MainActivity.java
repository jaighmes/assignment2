package edu.sjsu.android.zoodirectory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CustomAdapter.OnCatListener {

    private ArrayList<Cat> catList;
    RecyclerView recyclerView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch(item.getItemId()) {
            case R.id.information:
                intent = new Intent(this, InformationActivity.class);
                startActivity(intent);
                return true;
            case R.id.uninstall:
                intent = new Intent(Intent.ACTION_DELETE, Uri.parse("package:edu.sjsu.android.zoodirectory"));
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setCatList();

        recyclerView = findViewById(R.id.recyclerView);
        CustomAdapter adapter = new CustomAdapter(this, catList, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setCatList() {
        catList = new ArrayList<>();
        catList.add(new Cat("Siamese", "The Siamese is a long, elegant cat. The body is long, the neck is long, the legs and tail are long. The breed is medium sized but nicely muscled. ... The beauty of the Siamese cat is the look of the slender body, the blue eyes and the contrast between the color on the body and the darker color of the extremities.", R.drawable.siamese));
        catList.add(new Cat("Bengal", "The Bengal cat is a large, sleek and very muscular cat with a thick tail that is carried low. The Bengal cat's wild appearance is enhanced by its distinctive thick and luxurious spotted or marbled coat. Its broad head has small ears and pronounced whisker pads and its eyes are black rimmed and almond shaped.", R.drawable.bengal));
        catList.add(new Cat("Calico", "A calico cat is a domestic cat of any breed with a tri-color coat. The calico cat is most commonly thought of as being typically 25% to 75% white with large orange and black patches (or sometimes cream and grey patches); however, the calico cat can have any three colors in its pattern.", R.drawable.calico));
        catList.add(new Cat("Persian", "Persian cats are medium-sized, usually weigh between seven and 12 pounds, and measure from 10-15 inches tall. They have a rounded head, small, rounded ears, and big eyes. ... These long-haired cats can come in a multitude of patterns and colors, including white, black, blue, cream, chocolate, and red.", R.drawable.persian));
        catList.add(new Cat("Sphynx", "The sphynx is a medium-sized cat with a striking appearance, identified by her hairless, wrinkled skin and large ears. She has a sleek, muscular body that is dense and heavy for her size. ... Apart from being hairless, the sphynx's most notable feature is her large, triangle-shaped ears that resemble those of a bat.", R.drawable.sphynx));

    }

    @Override
    public void onCatClick(int position) {
        catList.get(position);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("name", catList.get(position).getName());
        intent.putExtra("description", catList.get(position).getDescription());
        intent.putExtra("image", catList.get(position).getImageID());
        if (position == catList.size() - 1) {
            openDialog(intent);
        } else {
            startActivity(intent);
        }
    }
    public void openDialog(Intent intent) {
        Dialog dialog = new Dialog(intent);
        dialog.show(getSupportFragmentManager(), "dialog");
    }
}