package edu.sjsu.android.zoodirectory;

public class Cat {

    private String name;
    private String description;
    private int imageID;

    public Cat(String name, String description, int imageID) {
        this.name = name;
        this.description = description;
        this.imageID = imageID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getImageID(){
        return imageID;
    }
}
