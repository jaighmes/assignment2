package edu.sjsu.android.zoodirectory;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {

    ImageView mainImageView;
    TextView name, description;

    String data1, data2;
    int imgID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mainImageView = findViewById(R.id.detailsIcon);
        name = findViewById(R.id.detailsName);
        description = findViewById(R.id.description);

        getData();
        setData();
    }

    private void getData() {
        if (getIntent().hasExtra("name") && getIntent().hasExtra("description") &&
                getIntent().hasExtra("image")) {
            data1 = getIntent().getStringExtra("name");
            data2 = getIntent().getStringExtra("description");
            imgID = getIntent().getIntExtra("image", 1);
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }

    }

    private void setData() {
        name.setText(data1);
        description.setText(data2);
        mainImageView.setImageResource(imgID);
    }
}
